show databases;
use college;
create table students(
id int not null auto_increment,name varchar(30) not null,
age int not null,primary key(id)
);
show tables;
select * from students;
select name from students;
describe students;
alter table students add gender varchar(10) not null after age;
alter table students add address varchar(50) not null,add contact int(12) not null;
alter table students modify city varchar(20) not null;
insert into students(name,age,gender,city,address,contact) values
("Billa",24,"M","NW","WA",0001),("STU",24,"M","NW","VA",0002),
("PRABA",30,"M","NW","GB",0003),("PHIL",30,"M","NW","LA",0004),
("JOHN",28,"M","NW","DM",0005),("Billa",24,"M","NW","KD",0006),
("CRYSTAL",22,"M","NW","KD",0007),("ALAN",42,"M","NW","WA",0000);
alter table studets rename to student;
show tables;
select name,age from student where city='kd';
describe student;
select * from student;
select name,age from student where city="NW";
select name,age from student where city="NW" and address="WA";
select name,age from student where (city="NW" or address="WA") and age>30;
select name,age from student where (city="NW" or address="KD") and age<30 order by age;
select distinct address from student;
select count(city) from student;
select count(distinct city) from student;
select count(distinct address) as address from student;
select *from student limit 0,5;
select * from student order by id desc limit 0,1; -- last in first
select max(age) from student;
select name,min(age) from student;
select name,sum(age) from student;
select gender,count(id) as total from student group by gender;
select name from student where name like "a%";
select name from student where name like "%a%";
select name from student where name like "%a";
select round(age,0) from student;
select *from student where city in ("NW");
select *from student where city not in ("NW");
select name from student where name not like "%a%";
select name,age from student where age between 20 and 30 order by id;
select * from student;
select name ,count(id) from student group by id; 
select name, count(id) as working,
 count(if(city='NW',1,null)) as Nework
 from student group by id;
create table emp (id int not null auto_increment,
				name varchar(30) not null,design varchar(30) not null,
                doj date not null,primary key(id));
                
 insert into emp (name,design,doj) values("Johnny","SE","2022-02-21"),("Mani","SE","2022-02-21"),
 ("Naveen","SE","2022-02-21"),("Joy","SE","2022-02-21"),("Hari","SE","2022-02-21"),("Lingesh","SE","2022-02-21"); 
 show tables; 
truncate table emp;
select * from emp;
create table salary(sid int not null auto_increment ,
 id int not null,sdate date not null,
 amt int not null,primary key(sid)); 
 
insert into salary(id,sdate,amt) values(1,"2022-02-21",200000),
			(2,"2022-02-21",200000),(3,"2022-02-21",200000),
            (4,"2022-02-21",200000),(5,"2022-02-21",200000),(6,"2022-02-21",200000);

select * from emp;
select emp.id,emp.name,emp.design,salary.id,salary.amt from 
emp right join salary 
on emp.id=salary.id;

