create database org;
use org;
create table student(rollno int not null,name varchar(30)not null,dept varchar(10)not null,primary key(rollno));
alter table student drop rollno;
alter table student add rollno int not null;
drop table student;
create table student(id int not null auto_increment,rollno int not null,name varchar(30)not null,dept varchar(10)not null,primary key(id));
insert into student(name,dept,rollno)value("Janagi Raman","401"),("Hari","402"),
("Mani","403"),("Navven","404"),("Lingesh","405"),("Joy","406");
-- update student set rollno="406" where name="Joy";
select * from student;
alter table student drop dept;
create table course(id int not null auto_increment,course varchar(10) not null,primary key(id));
insert into course(course) values("CSE"),("IT"),("MECH"),("EEE"),("CIVIL"),("CSE");
SELECT * FROM COURSE;
select student.name,student.rollno,course.course from student inner join course on student.id=course.id;
create table marks(id int not null auto_increment primary key,m1 int not null,m2 int not null,m3 int not null);
insert into marks(m1,m2,m3)values(31,10,97),(99,95,90),(80,90,80),(70,60,80),(50,70,100),(70,90,80);
select student.name,student.rollno,course.course,marks.m1,marks.m2,marks.m3 from 
 student inner join course on student.id=course.id inner join marks on student.id=marks.id;
alter table marks drop total;
describe marks;
select student.name,student.rollno,course.course,marks.m1,marks.m2,marks.m3,(marks.m1+marks.m2+marks.m3)as total,
 round((marks.m1+marks.m2+marks.m3)/3,2)as avgerage from student inner join course on student.id=course.id 
 inner join marks on marks.id=student.id;

create table product(id int not null auto_increment primary key,watch int not null,bag int not null);
alter table product change watch pname varchar(20)not null,change bag rate int not null ;
create table pricelog(id int not null auto_increment primary key, pid int not null,price int not null,newprice int not null);
insert into product(pname,rate) values("watch",600),("bag",800);
select * from product;
delimiter ##

create trigger modification 
 before update on product
 for each row 
 begin
  insert into pricelog(pid,price,newprice) values(old.id,old.rate,new.rate);
	end; ##
delimiter ; 
 update product set rate=10000 where id=3;

select * from product;
select * from pricelog;
-- create user & password
create user 'mani'@'localhost' identified by'123';
grant all on student to 'mani'@'localhost';
show grants;
select * from course;
create view grade1 as
select student.id,student.name,student.rollno,course.course,marks.m1,marks.m2,marks.m3,(marks.m1+marks.m2+marks.m3)as total,
 round((marks.m1+marks.m2+marks.m3)/3,2)as avgerage,
 case when marks.m1>=35 and marks.m2>=35 and marks.m3>=35 then 'pass' else 'fail'
 end as result from student inner join course on student.id=course.id inner join marks on student.id=marks.id;
 
 select * from grade;
 show full tables;
 create table names as select rollno,name from student;
 select * from names;
 select name from grade1 where exists(select name from student where student.id=grade1.id and total<=200);
 select name from student where rollno IS not NULL;
 select name from grade1 where result in('pass','fail');
 